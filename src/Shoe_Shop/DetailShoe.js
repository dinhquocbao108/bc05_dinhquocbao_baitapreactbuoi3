import React, { Component } from 'react'

export default class DetailShoe extends Component {
  render() {
    return (
      <div>
        <div className='flex'>
        <img src={this.props.detail.image} className='mx-auto' alt="" />
        <div className='my-auto'> 
            <p>{this.props.detail.name}</p>
            <p>{this.props.detail.price}</p>
            <p>{this.props.detail.description}</p>
            <p>{this.props.detail.shortDescription}</p>
        </div>
      </div>
      </div>
    )
  }
}
// import React, { useState } from 'react'

// export default function DetailShoe(props) {
//   const[hideDes,setHideDes]= useState(true)
//   // setHideDes(!hideDes)
//   return (
//     <div>
//       <div style={{
//         height:"100px",
//       }}>
//       {!hideDes&&   <p className=''>{props.detail.description}</p>}
//       </div>
//     </div>
//   )
// }

