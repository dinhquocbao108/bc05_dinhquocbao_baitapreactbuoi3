import React, { Component } from 'react'
import ItemShoe from './ItemShoe';

export default class ListShoe extends Component {
    renderListShoe=()=>{
        return this.props.shoeArr.map((item)=>{
            return <ItemShoe
            handleAddToCart={this.props.handleAddToCart}
             changeDetail={this.props.changeDetail}  data={item}/>
        });
    };
  render() {
    return (
      <div className='grid grid-cols-4'>
        {this.renderListShoe()}
      </div>
    )
  }
}
