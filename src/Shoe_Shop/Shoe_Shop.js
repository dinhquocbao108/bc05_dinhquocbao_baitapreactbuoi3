import React, { Component } from "react";
import Header from "./Header";
import { dataShoe } from "./dataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";
export default class Shoe_Shop extends Component {
  state = {
    dataShoe: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  plusNumber = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index !== -1) {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  minusNumber = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index !== -1 && cloneCart[index].number >= 1) {
      cloneCart[index].number--;
    }
    if (cloneCart[index].number == 0  ) {
      cloneCart.splice(index, 1);
    };
    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div>
        <Header />
        <div className="container px-40 mt-10">
          {/*Cart*/}
          <Cart
            plusNumber={this.plusNumber}
            minusNumber={this.minusNumber}
            cart={this.state.cart}
          />
          {/* render listShoe */}
          <ListShoe
            handleAddToCart={this.handleAddToCart}
            changeDetail={this.handleChangeDetail}
            shoeArr={this.state.dataShoe}
          />
          {/* detail Shoe */}
          <DetailShoe detail={this.state.detail} />
        </div>
      </div>
    );
  }
}
