import React, { Component } from 'react'

export default class Cart extends Component {
  renderTbody=()=>{
    return this.props.cart.map((item)=>{
      return <tr>
        <td>{item.id}</td>
        <td>{item.name}</td>
        <td>{item.price*item.number}</td>
        <td>
          <button 
          onClick={()=>{
            this.props.minusNumber(item)
          }}
           className='rounded-full w-8 bg-gray-400 mr-2' >-</button>
          {item.number}
          <button
          onClick={()=>{
            this.props.plusNumber(item)
          }}
           className='rounded-full w-8 bg-gray-400 ml-2'>+</button>
          </td>
        <td className='flex justify-center'><img style={{
          height:"80px",
        }} src={item.image} alt="" /></td>
      </tr>
    })


  };

  render() {
    return <table className='table-fixed' style={{
      width:"100%",
    }}>
      <thead>
        <tr>
          <th>ID</th>
          <th>NAME</th>
          <th>PRICE</th>
          <th>QUANTITY</th>
          <th>IMAGE</th>
        </tr>
      </thead>
      <tbody>{this.renderTbody()}</tbody>

    </table>
     
  }
}

